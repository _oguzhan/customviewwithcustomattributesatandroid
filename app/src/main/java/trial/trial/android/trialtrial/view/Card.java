package trial.trial.android.trialtrial.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import trial.trial.android.trialtrial.R;

/***
 * Ref : http://trickyandroid.com/protip-inflating-layout-for-your-custom-view/
 * Ref : https://stackoverflow.com/a/3441986/3341089
 */
public class Card extends RelativeLayout {
    private TextView header;
    private TextView description;
    private ImageView thumbnail;
    private ImageView icon;

    public Card(Context context) {
        super(context);
        init();
    }

    public Card(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Card);
        configure(typedArray);
    }

    public Card(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Card, defStyle, 0);
        configure(typedArray);
    }

    private void init() {
        inflate(getContext(), R.layout.card, this);
        this.header = (TextView) findViewById(R.id.title);
        this.description = (TextView) findViewById(R.id.description);
        this.thumbnail = (ImageView) findViewById(R.id.thumbnail);
        this.icon = (ImageView) findViewById(R.id.icon);
    }

    private void configure(TypedArray typedArray) {
        String title = typedArray.getString(R.styleable.Card_card_title);
        String description = typedArray.getString(R.styleable.Card_card_description);
        Drawable src = typedArray.getDrawable(R.styleable.Card_card_src);

        this.header.setText(title);
        this.description.setText(description);
        this.thumbnail.setImageDrawable(src);

        typedArray.recycle();
    }

}